#!/usr/bin/python
# -*- coding: utf-8 -*-

import redacao
import pickle

def read_texto(file):
    f = open(file+'.txt','r', encoding='utf8')
    return f.read()

def read_marcacao(file):
    f = open(file+'.ann','r', encoding='utf8')
    marcacoes = []
    relacoes = []
    
    for i in f.readlines():
        marc = i.split('\t')[1].split(' ')
        if marc[0] in ['Tese', 'Argumento', 'PI']:
            marcacoes.append(redacao.Marcacao(marc[0],marc[1], marc[2]))
        else:
            relacoes.append(redacao.Relacao(marc[0],marc[1].replace('Arg1:',''), marc[2].replace('Arg2:','')))
    return marcacoes, relacoes

def extracao(file, numero):
    redacoes = []

    for i in range(50):
        addZero = ''
        if i < 9:
            addZero = '0'

        txt = read_texto(file + 'red' + addZero + str(i+1)).split('\n')
        titulo = txt[0]
        texto = txt[1:]
        marcacoes, relacoes = read_marcacao(file + 'red' + addZero + str(i+1))
        
        redacoes.append(redacao.Redacao(titulo, texto, marcacoes, relacoes))
    pickle.dump(redacoes, open("redacoes"+numero+".pkl", "wb"))
    return redacoes  

def getRedacaoMarcada(redacao, tese, argumento, pi):
    qtd = 0
    texto = ' '.join(redacao.texto)
    redacao_kripp = ['*']*len(texto)
    for j in redacao.marcacao:
        qtd+=1
        for h in range(int(j.inicio)-len(redacao.titulo)-1,int(j.fim)-len(redacao.titulo)-1):
            if j.nome == 'Tese':
                redacao_kripp[h] = tese
            elif j.nome == 'Argumento':
                redacao_kripp[h] = argumento
            elif j.nome == 'PI':
                redacao_kripp[h] = pi
    return redacao_kripp, qtd