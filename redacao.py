class Redacao:  
    def __init__(self, titulo, texto, marcacao, relacao):
        self.titulo = titulo
        self.texto = texto
        self.marcacao = marcacao
        self.relacao = relacao

    def getQuantidadeDeMarcacao(self):
        return len(self.marcacao)

class Marcacao:
    def __init__(self, nome, inicio, fim):
        self.nome = nome
        self.inicio = inicio
        self.fim = fim
    
class Relacao:
    def __init__(self, nome, arg1, arg2):
        self.nome = nome
        self.arg1 = arg1
        self.arg2 = arg2

    def __str__(self):
        return self.nome
