#!/usr/bin/python
# -*- coding: utf-8 -*-

import redacao
import pickle
import numpy as np
import krippendorff
import preparation

BASE_CORPUS = 'corpus/'

def getKrippendorffNominal(array):
    krippendorffNominal = krippendorff.alpha(reliability_data=array, level_of_measurement='nominal')
    if (np.isnan(krippendorffNominal)):
        return 1
    return krippendorffNominal

def comparaRedacoes(redacao1, redacao2, tese, argumento, pi):
    array = []
    array.append([np.nan if v == '*' else int(v) for v in preparation.getRedacaoMarcada(redacao1,tese,argumento,pi)[0]])
    array.append([np.nan if v == '*' else int(v) for v in preparation.getRedacaoMarcada(redacao2,tese,argumento,pi)[0]])
    return getKrippendorffNominal(array)

if __name__ == '__main__':
    redacoes1 = preparation.extracao(BASE_CORPUS+'anotador1/','1')
    redacoes2 = preparation.extracao(BASE_CORPUS+'anotador2/','2')

    resultados = {'geral': [], 'tese': [], 'argumento': [], 'pi': []}

    for i in range(len(redacoes1)):
        
        nominal = comparaRedacoes(redacoes1[i], redacoes2[i], 1, 2, 3)
        resultados['geral'].append(nominal)
        
        nominal = comparaRedacoes(redacoes1[i], redacoes2[i], 1, 2, 2)
        resultados['tese'].append(nominal)

        nominal = comparaRedacoes(redacoes1[i], redacoes2[i], 2, 1, 2)
        resultados['argumento'].append(nominal)
        
        nominal = comparaRedacoes(redacoes1[i], redacoes2[i], 2, 2, 1)
        resultados['pi'].append(nominal)

    for resultado in resultados:
        print(f'Teste do componente {resultado: <10}' + ' %.3f' % np.mean(np.array(resultados[resultado]), dtype=np.float64))

    